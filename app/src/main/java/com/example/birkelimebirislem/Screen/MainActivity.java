package com.example.birkelimebirislem.Screen;

import android.os.Bundle;
import android.os.Handler;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.example.birkelimebirislem.R;
import com.example.birkelimebirislem.Screen.Fragment.Fragment_Main;
import com.example.birkelimebirislem.Screen.Fragment.Fragment_Player_Math;

public class MainActivity extends AppCompatActivity {


    public static MainActivity instance;//singeltn

    ProgressBar progressBarWord,progressBarTransaction;
    private int mProgressStatus = 0;
    private Handler mhandler = new Handler();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

       Init();

        instance = this;
        loadFragment(new Fragment_Main());


    }
    public void Init()
    {
        progressBarWord = findViewById(R.id.progressBarWord);
        progressBarTransaction = findViewById(R.id.progressbarTransaction);
    }



    public boolean loadFragment(Fragment fragment) {
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.frame_container, fragment).addToBackStack(null)
                    .commit();
            return true;
        }
        return false;

    }

}
