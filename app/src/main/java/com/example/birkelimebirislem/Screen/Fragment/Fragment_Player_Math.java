package com.example.birkelimebirislem.Screen.Fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.example.birkelimebirislem.Helper.Math.Expression;
import com.example.birkelimebirislem.Screen.MainActivity;
import com.example.birkelimebirislem.R;
import com.example.birkelimebirislem.Helper.Rastgele;

import java.util.List;


public class Fragment_Player_Math extends Fragment{

    TextView txtRandomNumber,txtResult;
    EditText txtLog;
    Button btnWord;
    Button[] buttons = new Button[6];
    List<Integer> number;

    int letterLength;
    int findedNumber;
    public Fragment_Player_Math(int letterLength)
    {
        this.letterLength = letterLength;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_transaction, container, false);

        init(view);
        randomNumber();
       List<Integer> a = Expression.Parse("12+42");
       for (int i = 0 ; i<a.size();i++)
       {
           Log.d("deneme",a.get(i)+"");
       }
        txtLog.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if(!NumberControl() && !NumberOutControl())
                {

                    if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                        findedNumber = (int)Expression.Calculate(txtLog.getText().toString());
                        txtResult.setText(""+findedNumber);

                    }
                }

                return false;
            }


        });

        btnWord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(),findedNumber+"-"+number.get(6),Toast.LENGTH_LONG).show();
                int result = number.get(6)-findedNumber;
               result =  Math.abs(result);
                if(result <10 && result> -10)
                    MainActivity.instance.loadFragment(new Fragment_Point(letterLength,result));
                else
                    Toast.makeText(getContext(),"Sonuca malesef yaklaşamadınız",Toast.LENGTH_SHORT).show();
            }
        });
        return view;
    }

    private boolean NumberOutControl() {
        List<Integer> sayilar = Expression.Parse(txtLog.getText().toString());
        int index = -1;
        for (int j = 0; j < sayilar.size(); j++) {
            Log.d("DDD",sayilar.get(j)+"");
            index = HelperIndexFind(sayilar.get(j));

            if (index == -1) {
                break;
            }
        }
        return index == -1 ? true : false;
    }

    private boolean NumberControl()
    {
        List<Integer> sayilar = Expression.Parse(txtLog.getText().toString());
        boolean result = false;
        for (int i=0; i<number.size(); i++)
        {
            int sayac=0;

            for (int j=0; j<sayilar.size();j++)
            {
                Log.d("ZZZZ",number.get(i)+"=="+sayilar.get(j));
                if(number.get(i).equals(sayilar.get(j)))
                {
                    sayac ++;
                }
            }
            if(sayac>1){
                result = true;
                Toast.makeText(getContext(),"Bu sayıyı birden fazla kullanamazsınız.",Toast.LENGTH_LONG).show();

            }
        }
        return result;
    }


    private void randomNumber() {

        number = Rastgele.Uret();

        for(int k = 0 ; k<6;k++){
            buttons[k].setText(number.get(k).toString());
        }

        txtRandomNumber.setText(number.get(6).toString());

        }


    private void init(View view) {

        buttons[0] = view.findViewById(R.id.transaction_btn1);
        buttons[1] = view.findViewById(R.id.transaction_btn2);
        buttons[2] = view.findViewById(R.id.transaction_btn3);
        buttons[3] = view.findViewById(R.id.transaction_btn4);
        buttons[4] = view.findViewById(R.id.transaction_btn5);
        buttons[5] = view.findViewById(R.id.transaction_btn6);
        txtRandomNumber = view.findViewById(R.id.transaction_txtRandom_number);
        txtLog = view.findViewById(R.id.transaction_edtLog);
        txtResult = view.findViewById(R.id.transaction_txtResult);
        btnWord = view.findViewById(R.id.transaction_btnWord);
    }


    private int HelperIndexFind(int num){
        int result = -1;
        for (int i=0; i<number.size(); i++)

        {
            if(number.get(i) == num)
            {
                result = i;
                break;
            }
        }

        return result;
    }

}
