package com.example.birkelimebirislem.Screen.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import androidx.fragment.app.Fragment;

import com.example.birkelimebirislem.Screen.MainActivity;
import com.example.birkelimebirislem.R;


public class Fragment_Main extends Fragment {

    ImageButton btngamer, btnAi;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);

        btngamer=view.findViewById(R.id.btnGamer);
        btnAi=view.findViewById(R.id.btnAi);

        btngamer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.instance.loadFragment(new Fragment_AI_Word());
            }
        });
        btnAi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.instance.loadFragment(new Fragment_Player_Word());
            }
        });
        return view;
    }



}
