package com.example.birkelimebirislem.Screen.Fragment;

import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.birkelimebirislem.Helper.Word.Word;
import com.example.birkelimebirislem.R;
import com.example.birkelimebirislem.Screen.MainActivity;

import java.util.*;


public class Fragment_AI_Word extends Fragment {

    View view;


    public Button nextPage;
    public List<Button> wordBtnList = new ArrayList<>();
    public List<TextView> alternativeResult = new ArrayList<>();

    TextView result;

    Button btnQuestion;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_ai__word, container, false);
        Initialize();
        long startTime = System.currentTimeMillis();
        Word game = new Word(getContext());
        game.CreateGame();
        List<String > resultArray = game.Find();
        String[] lettersArray = game.GetLetters();

        for (int i = 0 ;i <lettersArray.length;i++)
        {
            wordBtnList.get(i).setText(lettersArray[i]);
        }

        result.setText(resultArray.get(0));

        for (int k = 0 ; k<alternativeResult.size()-1;k++)
        {
            alternativeResult.get(k).setText(resultArray.get(k));
        }
        long endTime = System.currentTimeMillis();
        // bitiş
        long estimatedTime = endTime - startTime;     // Geçen süre milisaniye cinsinden elde edilir
        double seconds = (double)estimatedTime/1000; // saniyeye çevirmek için 1000'e bölüyoruz.


        Log.d("FFFF",seconds+"");





        return view;
    }

    private void Initialize()
    {
        btnQuestion=view.findViewById(R.id.word_btn9);
        wordBtnList.add(view.findViewById(R.id.word_btn1));
        wordBtnList.add(view.findViewById(R.id.word_btn2));
        wordBtnList.add(view.findViewById(R.id.word_btn3));
        wordBtnList.add(view.findViewById(R.id.word_btn4));
        wordBtnList.add(view.findViewById(R.id.word_btn5));
        wordBtnList.add(view.findViewById(R.id.word_btn6));
        wordBtnList.add(view.findViewById(R.id.word_btn7));
        wordBtnList.add(view.findViewById(R.id.word_btn8));
        wordBtnList.add(btnQuestion);


        alternativeResult.add(view.findViewById(R.id.word_txt1));
        alternativeResult.add(view.findViewById(R.id.word_txt2));
        alternativeResult.add(view.findViewById(R.id.word_txt3));
        alternativeResult.add(view.findViewById(R.id.word_txt4));
        alternativeResult.add(view.findViewById(R.id.word_txt5));
        alternativeResult.add(view.findViewById(R.id.word_txt6));
        alternativeResult.add(view.findViewById(R.id.word_txt7));
        alternativeResult.add(view.findViewById(R.id.word_txt8));

        result = view.findViewById(R.id.fragmentWord_txtWord);

        nextPage =view.findViewById(R.id.word_btnNextPage);

        nextPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.instance.loadFragment(new Fragment_AI_Math(result.getText().toString().length()));
            }
        });
    }

    private void FakeGame()
    {
        Log.e("FF","[FAKE GAME]");
        for (int i = 0 ; i<10;i++)
        {
            Word game = new Word(getContext());
            game.CreateGame();
            List<String > resultArray = game.Find();
            String[] lettersArray = game.GetLetters();

            for (int k = 0 ;k <lettersArray.length;k++)
            {
               Log.d("Letters : ",lettersArray[k]);
            }

            if (resultArray.size()>0)
            Log.d("Result : ",resultArray.get(0));

        }
    }


}
