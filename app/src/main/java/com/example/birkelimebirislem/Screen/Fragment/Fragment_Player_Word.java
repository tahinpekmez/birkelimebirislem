package com.example.birkelimebirislem.Screen.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.example.birkelimebirislem.Helper.Word.Word;
import com.example.birkelimebirislem.R;
import com.example.birkelimebirislem.Screen.MainActivity;

import java.util.*;


public class Fragment_Player_Word extends Fragment {

    List<Button> btnWords = new ArrayList<>();
    List<String> words = new ArrayList<>();
    String[] letters;



    TextView txtLogWord, txtWord;
    Button btnWordJoker,btnNextPage,btnQuestion;

    ImageButton imgSave, imgClear;
    Word game;

    boolean findedWord = false;
    boolean isWordJoker=true;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_word, container, false);

        Initialize(view);
        InitializeGame();
        InitializeClickEvent();

        return view;
    }

    private void InitializeGame()
    {
        game = new Word(getContext());
        game.CreateGame();
        words = game.GetWordsList();
        letters = game.GetLetters();

        for (int i = 0; i <= 7; i++) {

            btnWords.get(i).setText(letters[i]);
        }
    }

    private void Initialize(View view)
    {

        btnWords.add(view.findViewById(R.id.word_btn1));
        btnWords.add(view.findViewById(R.id.word_btn2));
        btnWords.add(view.findViewById(R.id.word_btn3));
        btnWords.add(view.findViewById(R.id.word_btn4));
        btnWords.add(view.findViewById(R.id.word_btn5));
        btnWords.add(view.findViewById(R.id.word_btn6));
        btnWords.add(view.findViewById(R.id.word_btn7));
        btnWords.add(view.findViewById(R.id.word_btn8));


        btnWordJoker = view.findViewById(R.id.word_btn9);
        btnNextPage = view.findViewById(R.id.word_btnNextPage);

        txtLogWord = view.findViewById(R.id.fragmentWord_txtLogWord);
        txtWord = view.findViewById(R.id.fragmentWord_txtWord);


        imgSave = view.findViewById(R.id.word_imgCheck);
        imgClear = view.findViewById(R.id.word_imgDelete);
    }

    public void InitializeClickEvent()
    {
        btnWordJoker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isWordJoker){
                    isWordJoker=false;
                    txtLogWord.setText(txtLogWord.getText().toString() + btnWordJoker.getText().toString());
                }else{
                    Toast.makeText(getContext(), "Joker Hakkını Kullandınız", Toast.LENGTH_SHORT).show();
                }



            }
        });

        for (int k = 0; k < btnWords.size(); k++) {
            final int a = k;
            btnWords.get(a).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    txtLogWord.setText(txtLogWord.getText().toString() + btnWords.get(a).getText().toString());

                    //btnWords.get(a).setEnabled(false);
                }

            });


        }

        imgSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                HelperWordButtonActive(true);

                btnWordJoker.setEnabled(true);
                Toast.makeText(getContext(), txtLogWord.getText(), Toast.LENGTH_SHORT).show();

                findedWord = game.FindPlayer(txtLogWord.getText().toString());
                if (findedWord)
                {
                    Toast.makeText(getContext(), "Harika bir sonuç buldun.", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(getContext(), "Sonuç Bulunamadı.", Toast.LENGTH_SHORT).show();
                }

            }


        });

        imgClear.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                txtLogWord.setText(null);
                isWordJoker=true;
                HelperWordButtonActive(true);

                btnWordJoker.setEnabled(true);

            }

        });
        

        btnNextPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(findedWord)
                {
                    MainActivity.instance.loadFragment(new Fragment_Player_Math(txtLogWord.getText().length()));
                }
            }
        });
    }


    private void HelperWordButtonActive(boolean visibility)
    {
        for (int n = 0; n < btnWords.size(); n++) {

            btnWords.get(n).setEnabled(visibility);
        }
    }






}
