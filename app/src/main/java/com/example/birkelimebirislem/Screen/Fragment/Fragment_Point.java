package com.example.birkelimebirislem.Screen.Fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import android.widget.Button;
import android.widget.TextView;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.birkelimebirislem.Helper.PointCalculator;
import com.example.birkelimebirislem.R;


public class Fragment_Point extends Fragment {

    TextView txtLetterPoint,txtNumberPoint ;
    Button homePage;
    View view;
    int letterPoint,numberPoint;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_puan, container, false);
        Initialize();
        SetData();
        return view;
    }

    public Fragment_Point(int correctLetter,int number)
    {
        letterPoint = PointCalculator.WordCalc(correctLetter);
        numberPoint = PointCalculator.NumberCalc(number);
    }

    private void SetData()
    {
        txtLetterPoint.setText(""+letterPoint);
        txtNumberPoint.setText(""+numberPoint);
    }

    private void Initialize()
    {
        txtLetterPoint = view.findViewById(R.id.point_txt_word);
        txtNumberPoint = view.findViewById(R.id.point_txt_number);
    }

}
