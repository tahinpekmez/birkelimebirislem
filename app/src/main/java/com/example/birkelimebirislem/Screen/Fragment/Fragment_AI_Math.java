package com.example.birkelimebirislem.Screen.Fragment;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.birkelimebirislem.Helper.Math.Game;
import com.example.birkelimebirislem.R;
import com.example.birkelimebirislem.Screen.MainActivity;

public class Fragment_AI_Math extends Fragment {



    Button btnNextPage;
    Button[] buttonNumbersList = new Button[6];
    TextView[] alternativeResult = new TextView[8];
    TextView textViewTargetNumber;
    TextView textViewResult;
    EditText log;

    Game gm = new Game();
    View view ;
    int letterLength;
    int findedNumber =0;

    public Fragment_AI_Math(int letterLength)
    {
        this.letterLength = letterLength;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_ai, container, false);
        InitializeGame();
        InitializeNumber();



        return view;

    }

    private void InitializeNumber()
    {
        textViewTargetNumber = view.findViewById(R.id.transaction_txtRandom_number);
        textViewResult = view.findViewById(R.id.transaction_txtResult);
        buttonNumbersList[0] = view.findViewById(R.id.transaction_btn1);
        buttonNumbersList[1] = view.findViewById(R.id.transaction_btn2);
        buttonNumbersList[2] = view.findViewById(R.id.transaction_btn3);
        buttonNumbersList[3] = view.findViewById(R.id.transaction_btn4);
        buttonNumbersList[4] = view.findViewById(R.id.transaction_btn5);
        buttonNumbersList[5] = view.findViewById(R.id.transaction_btn6);

        btnNextPage = view.findViewById(R.id.transaction_btnWord);
        btnNextPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MainActivity.instance.loadFragment(new Fragment_Point(letterLength,gm.gameData.targetNumber-findedNumber));
            }
        });




        log = view.findViewById(R.id.transaction_edtLog);


        for (int i = 0;i<5;i++)
        {
            buttonNumbersList[i].setText(gm.gameData.numbers[i]+"");
        }
        buttonNumbersList[5].setText(gm.gameData.twoDigitNumber+"");

        textViewTargetNumber.setText(gm.gameData.targetNumber+"");
        findedNumber = gm.FindNumber();
        textViewResult.setText(findedNumber+"");
    }

    private void InitializeGame()
    {

        gm.CreateGame();



    }
}
