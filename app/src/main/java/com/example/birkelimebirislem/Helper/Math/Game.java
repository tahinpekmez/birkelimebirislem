package com.example.birkelimebirislem.Helper.Math;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;


public class Game {

    public GameData gameData;
    ArrayList<Integer> numberList;
    Finder f = new Finder();
    public void CreateGame()
    {

        gameData = new GameData();

        gameData.targetNumber = RandomNumber(100,999);
        gameData.numbers = RandomNumbers();
        gameData.twoDigitNumber =RandomNumber(10,99);
        gameData.twoDigitNumber -= gameData.twoDigitNumber %10;
        numberList = new ArrayList<>();

        for (int i = 0 ; i<gameData.numbers.length;i++)
        {
            numberList.add(gameData.numbers[i]);
        }
        numberList.add(gameData.twoDigitNumber);

    }

    public int FindNumber()
    {
        int tamBolen = (gameData.targetNumber - gameData.targetNumber%gameData.twoDigitNumber)/gameData.twoDigitNumber;
        int kalanSayi = gameData.targetNumber - tamBolen*gameData.twoDigitNumber;
        int result = -1;
        if(f.FindNumber(numberList,tamBolen))
        {
            if(f.FindNumber(numberList,kalanSayi))
            {
                result = kalanSayi;
            }
        }

        result +=tamBolen*gameData.twoDigitNumber;
        return result;
    }

    private static int RandomNumber(int min,int max)
    {
        final int random = new Random().nextInt((max - min) + 1) + min;




        return random;
    }
    private static int[] RandomNumbers()
    {
        int[] randomNumber = new int[6];
        ArrayList<Integer> list = new ArrayList<Integer>();
        for (int i=1; i<10; i++) {
            list.add(new Integer(i));
        }
        Collections.shuffle(list);
        for (int i=0; i<6; i++) {
            randomNumber[i] = list.get(i);
        }


        return randomNumber;

    }



}
