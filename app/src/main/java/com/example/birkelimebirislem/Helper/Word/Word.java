package com.example.birkelimebirislem.Helper.Word;

import android.content.Context;
import android.os.Build;
import android.util.Log;
import androidx.annotation.RequiresApi;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Word {

    List<String> words = new ArrayList<>();
    List<String> letters = new ArrayList<String>();
    List<String> jokerList = new ArrayList<>();
    public void CreateGame() {
        InitializeWords();
        RandomLetters();
    }

    Context context;
    public Word(Context context)
    {
        this.context = context;
    }

    public List<String> Find() {

        List<String> result = new ArrayList<>();
        for (int i = 0; i < words.size(); i++) {

            char[] data = words.get(i).toLowerCase().toCharArray();
            int counter = 0;
            boolean joker = false;
            for (int j = 0; j < data.length; j++) {

                int index = letters.indexOf(data[j]+"");
                if (index == -1) {
                    if (!joker) {
                        counter++;
                        joker = true;
                    }

                } else {
                    counter++;
                }


            }



            if (counter == data.length) {

                result.add(words.get(i));



            }


        }
        Sort(result);

        return result;
    }

    public boolean FindPlayer( String target)
    {

        List<String> result = new ArrayList<>();
        for (int i = 0; i < words.size(); i++) {

            char[] data = words.get(i).toLowerCase().toCharArray();
            char[] targetData = target.toLowerCase().toCharArray();

            int counter = 0;
            boolean joker = false;
            for (int k = 0 ; (k<targetData.length && k<data.length);k++)
            {
                if (targetData[k] == data[k])
                {
                    counter++;
                }else if (targetData[k] == '?' && !joker)
                {
                    counter++;
                }
            }


            if (counter == targetData.length)
                return true;

        }

        return false;






    }


    public String[] GetLetters() {
        String[] deneme = new String[letters.size()];
        letters.toArray(deneme);
        return deneme;
    }

    public String GetJokerLetter() {
        return jokerList.get(0);
    }


    private void InitializeWords() {

        BufferedReader reader = null;
        try {
            reader = new BufferedReader(
                    new InputStreamReader(context.getAssets().open("wordList.txt"), "UTF-8"));

            // do reading, usually loop until end of file reading
            String mLine;
            while ((mLine = reader.readLine()) != null) {
                words.add(mLine.trim().toLowerCase());
            }
        } catch (IOException e) {

        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    //log the exception
                }
            }
        }
        Sort(words);
    }

    private void RandomLetters() {
        List<String> randomLetter = new ArrayList<String>( Arrays.asList("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"));
        letters.clear();
        for (int i = 0; i < 8; i++) {
            Random rdm = new Random();
            int index = rdm.nextInt((randomLetter.size()-1 - 0) + 1) + 0;;
            letters.add(randomLetter.get(index).toLowerCase());
            randomLetter.remove(index);
        }

    }

    private static void Sort(List<String> list) {
        String temp;
        for (int j = 0; j <= list.size() - 2; j++) {
            for (int i = 0; i <= list.size() - 2; i++) {
                if (list.get(i).length() < list.get(i+1).length()) {
                    temp = list.get(i + 1);
                    list.set(i + 1,list.get(i));
                    list.set(i,temp);
                }
            }
        }
    }

    public List<String> GetWordsList()
    {
        InitializeWords();
        Sort(words);
        return words;
    }


}
