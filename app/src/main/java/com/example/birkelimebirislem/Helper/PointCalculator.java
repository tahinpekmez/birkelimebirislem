package com.example.birkelimebirislem.Helper;

public class PointCalculator
{
    public static int WordCalc(int correctLetter)
    {
        switch (correctLetter)
        {
            case 3:
            case 4:
            case 5:
                return correctLetter;
            case 6:
                return 7;
            case 7:
                return 9;
            case 8:
                return 11;
            case 9:
                return 15;
        }
        return 0;
    }

    public static int NumberCalc(int target)
    {
       if (target>5)
       {
           return target;
       }
       else if (target == 0)
       {
           return 20;
       }
       else
           return target+3;
    }
}
