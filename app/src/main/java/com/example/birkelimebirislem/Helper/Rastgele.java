package com.example.birkelimebirislem.Helper;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Rastgele {

    public static List<Integer> Uret()
    {
        List<Integer> list = new ArrayList<>();

        for (int i = 0 ; i<5;i++)
        {
            while(true)
            {
                int sayi = (new Random()).nextInt(9)+1;
                if(list.indexOf(sayi) == -1)
                {

                    list.add(sayi);
                    break;
                }

            }

        }
        int sayi = (new Random()).nextInt(90)+10;
        list.add(sayi - (sayi %10));
        sayi = (new Random()).nextInt(900)+100;
        list.add(sayi);

        return list;
    }
}
